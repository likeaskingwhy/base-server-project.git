module.exports = app => {

    const assert = require('http-assert')
    const jwt = require('jsonwebtoken')
    const Menu = require('../../models/system/Menu')
    const User = require('../../models/system/User')
    const Role = require('../../models/system/Role')
    const { handleTree } = require('../../utils/tools')
    const auth = require('../../middleWares/auth')

    /**
     * @description 新增菜单
     * */
    app.post('/menu', auth('system:menu:insert'), async (req, res) => {
        const {menuCode, menuType, path} = req.body
        const menu = await Menu.findOne({menuCode})
        assert(!menu, 422, '此菜单编码已被使用，请修改后重试。')
        if (menuType === 'M') {
            req.body.component = 'Layout'
        }
        if (menuType !== 'F') req.body.name = path.slice(0, 1).toUpperCase() + path.slice(1)

        await Menu.create(req.body)

        res.send({
            code: 200,
            message: '操作成功'
        })
    })

    /**
     * @description 获取菜单树
     * */
    app.get('/menu', auth('*:*:*'), async (req, res) => {

        const menus = await Role
            .find({_id: {$in: req.user.roles}})
            .distinct('permissions')
        const query = {
            status: '1',
            menuType: {$ne: 'F'}
        }

        if (req.user.userName !== 'admin') query._id = {$in: menus}

        const rows = await Menu
            .find(query)
            .sort({order: 1})
        const routers = rows.map(item => {
            let obj = {
                component: item.component,
                meta: {
                    title: item.title,
                    icon: item.icon,
                    isCache: item.isCache
                },
                menuType: item.menuType,
                name: item.name,
                menuCode: item.menuCode,
                parentCode: item.parentCode,
                hidden: item.visible === '0',
                path: item.path
            }
            if (item.menuType === 'M') {
                obj.redirect = 'noRedirect'
                if (item.parentCode !== '0') {
                    obj.component = 'ParentView'
                }
                else {
                    obj.component = item.component
                    obj.path = '/' + item.path
                }
            } else if (item.menuType === 'C') {
                obj.path = item.path
            }
            return obj
        })
        const tree = handleTree(routers, 'menuCode', 'parentCode')
        res.send({
            code: 200,
            message: '操作成功',
            data: tree
        })
    })

    /**
     * @description 查询菜单列表
     * */
    app.get('/menu/list', auth('system:menu:list'), async (req, res) => {
        const {title} = req.query
        const regexp = new RegExp(title,'i')
        const query = {
            $or: [{title: {$regex: regexp}}],
        }
        const data = await Menu
            .find(query)
            .sort({menuCode: 1})

        res.send({
            code: 200,
            message: '操作成功',
            data
        })
    })

    /**
     * @description 查询菜单详情信息
     * */
    app.get('/menu/:menuCode', auth('system:menu:detail'), async (req, res) => {
        const data = await Menu.findOne({menuCode: req.params.menuCode})

        res.send({
            code: 200,
            message: '操作成功',
            data
        })
    })

    /**
     * @description 修改菜单信息
     **/
    app.put('/menu', auth('system:menu:update'), async (req, res) => {
        const {_id: id} = req.body
        assert(id, 422, '编辑失败，请检查参数是否有效。')
        await Menu.findByIdAndUpdate(id, req.body)

        res.send({
            code: 200,
            message: '操作成功'
        })
    })

    /**
     * @description 删除菜单信息
     * */
    app.delete('/menu/:menuCode', auth('system:menu:delete'), async (req, res) => {
        const menu = await Menu.findOneAndDelete({menuCode: req.params.menuCode})
        assert(menu, 422, '删除失败，请检查参数是否有效。')

        res.send({
            code: 200,
            message: '操作成功'
        })
    })
}