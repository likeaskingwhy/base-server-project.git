const express = require('express')

const app = express()

app.use('/', express.static('public'))
app.use('/uploads', express.static(__dirname + '/uploads'))
app.use(express.json())

app.set('SECRET', 'SMART_SCHOOL_MANAGE_PLATFORM_2021_0815')

require('./plugins/db')(app)
require('./routes')(app)

const { getIPAdress } = require('./utils/ipConfig')

const host = getIPAdress()
const port = 8125

app.listen(port, host, () => {
    console.log(`http://${host}:${port} has been already started ...`);
})

// app.listen(8088, () => {
//     console.log(`8088 has been already started ...`);
// })
